# Loogup

Set up the development environment:
- Clone the repository
> git clone https://gitlab.com/VRoxa/loogup.git
- Install node.js dependencies
> npm install
- Run the service
Transpiles Typescript to ./dist folder. Copies client files and runs the service.
> npm run run