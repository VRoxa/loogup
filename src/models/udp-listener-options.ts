export class UDPListenerOptions {
    port?: number = 878;
    host?: string = '127.0.0.1';
    bindingCallback: (() => void) | undefined;
}