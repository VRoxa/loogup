import { LogLevel } from "./log-level";

export class LogMessage {
    Timestamp: Date | undefined;
    Context: string | any;
    Level: LogLevel | string | undefined;
    Message: string | undefined;
    Thread: any;
    Exception: any;
}