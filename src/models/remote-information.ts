export class RemoteInformation {
    address: string | undefined;
    family: familyType | undefined;
    port: number | undefined;
    size: number | undefined;
}

type familyType = 'IPv4' | 'IPv6';