import { UDPListenerOptions } from "../models/udp-listener-options";
import { Capturer } from "../capturer/capturer";
import { RemoteInformation } from "../models/remote-information";

type event = 'connect' | 'error' | 'close' | 'listening';

export class UDPListener {
    private _dgram = require('dgram');
    private _server: any;

    constructor(capturer: Capturer, private options?: UDPListenerOptions) {
        this._server = this._dgram.createSocket('udp4');
        this._server.on('message', capturer.capture);
    }

    public bind = (): UDPListener => {
        const options = this.options ?? new UDPListenerOptions();
        this._server.bind(options.port, options.host, options.bindingCallback);
        return this;
    }

    public on = (event: event, callback: (...args: any[]) => void): UDPListener => {
        this._server.on(event, callback);
        return this;
    }
    
    public get address(): RemoteInformation {
        const address = this._server.address();
        return <RemoteInformation>address;
    }
    
}
