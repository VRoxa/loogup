import { RemoteInformation } from "../models/remote-information";
import { LogMessage } from "../models/log-message";

export class Capturer {

    constructor(private onCaptured: (msg: LogMessage, rinfo: RemoteInformation) => void) { }

    private _capture = (msg: Buffer, rinfo: RemoteInformation) => {
        const logMessage: LogMessage = this.serializeBuffer<LogMessage>(msg);
        this.onCaptured(logMessage, rinfo);
    };

    private serializeBuffer<TResult>(buffer: Buffer): TResult {
        const raw: string = buffer.toString();
        const serialized: any = JSON.parse(raw);

        let result: TResult = Object.create(null);
        result = Object.assign(result, serialized);

        return result;
    }

    public get capture(): (msg: Buffer, rinfo: RemoteInformation) => void {
        return this._capture;
    }
}