import { UDPListener } from "./listener/listener";
import { RemoteInformation } from "./models/remote-information";
import { Capturer } from "./capturer/capturer";
import { LogMessage } from "./models/log-message";
import { LogLevel } from "./models/log-level";

// UDP
(() => {
    const capturer = new Capturer((msg: LogMessage, rinfo: RemoteInformation) => {
        console.table(msg);
    });

    const listener = new UDPListener(capturer, {
        port: 878,
        host: '127.0.0.1',
        bindingCallback: () => { console.log('DGram binded to UDP') }
    });
    listener.on('listening', () => {
        const address: RemoteInformation = listener.address;
        console.log(`Listening on ${address.address}:${address.port} ...`);
    })
    .on('error', err => {
        console.error(`An error occurred`, err);
    })
    .on('close', () => {
        console.log(`Socket closed`);
        console.warn(`No new 'message' events will be emited from now`);
    })
    .bind();
})();

// Web Socket server
(() => {
    var app = require('express')();
    var http = require('http').createServer(app);
    var io = require('socket.io')(http);

    app.get('/', (req: any, res: any) => {
        res.sendFile(__dirname + `/client/index.html`);
    });
    
    io.on('connection', (socket: any) => {
        console.log('User connected');

        socket.on('disconnect', () => {
            console.log('User disconnected...');
        })
    });

    http.listen(3000, () => {
        console.log(`Listening on *:3000`);
    });
})();
